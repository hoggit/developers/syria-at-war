local function logisticAssets()

	local function ARCO()
	  ARCOSpawner = SPAWN:NewWithAlias("ARCO", "ARCO")
	  :InitLimit(1, 0)
	  :SpawnScheduled(10, 0)
	  :InitRepeatOnLanding()

	  
	end

	local function PETROL()
		PETROLSpawner = SPAWN:NewWithAlias("Petrol", "Petrol")
		:InitLimit(1, 0)
		:SpawnScheduled(10, 0)
		:InitRepeatOnLanding()
  
		
	  end
	 
	local function SHELL()
	  SHELLSpawner = SPAWN:NewWithAlias("SHELL", "SHELL")
	  :InitLimit(1, 0)
	  :SpawnScheduled(10, 0)
	  :InitRepeatOnLanding()

	end
	 
	local function TEXACO()
	  TEXACOSpawner = SPAWN:NewWithAlias("TEXACO", "TEXACO")
	  :InitLimit(1, 0)
	  :SpawnScheduled(10, 0)
	  :InitRepeatOnLanding()

	end

	local function DARKSTAR()
	  DARKSTARSpawner = SPAWN:NewWithAlias("DARKSTAR", "DARKSTAR")
	  :InitLimit(1, 0)
	  :SpawnScheduled(10, 0)
	  :InitRepeatOnLanding()
	  
	end
	
	local function WIZARD()
	  WIZARDSpawner = SPAWN:NewWithAlias("WIZARD", "WIZARD")
	  :InitLimit(1, 0)
	  :SpawnScheduled(10, 0)
	  :InitRepeatOnLanding()
	  
	end

	local function IMAGE()
		IMAGEpawner = SPAWN:NewWithAlias("IMAGE", "IMAGE")
		:InitLimit(1, 0)
		:SpawnScheduled(10, 0)
		:InitRepeatOnLanding()
		
	  end

	local function RECOVERY()
		RECOVERYSpawner = SPAWN:NewWithAlias("Recovery Tanker", "Recovery Tanker")
		:InitLimit(1, 0)
		:SpawnScheduled(10, 0)
		:InitRepeatOnLanding()
		
	  end
	 
	ARCO()
	SHELL()
	TEXACO()
	DARKSTAR()
	WIZARD()
	RECOVERY()
	PETROL()
	IMAGE()

end
logisticAssets()
